#!/usr/bin/env python
from dumpster_lib.dumpster import RobotControl
import rospy
import math

robot = RobotControl("dumpster_truck")

robot.move_distance(1.0)
robot.move_distance(1.0)
robot.move_distance(0.5)
robot.rotate_angle(-math.pi/2.0)
robot.move_distance(0.5)
robot.rotate_angle(math.pi)
"""
for j in range(0,2):
    for i in range(0,5):
        robot.move_distance(1.0)
    robot.rotate_angle(-math.pi/2.0)
    robot.move_distance(1.0)
    robot.rotate_angle(-math.pi/2.0)
    for i in range(0,5):
        robot.move_distance(1.0)
    robot.rotate_angle(math.pi/2.0)
    robot.move_distance(1.0)
    robot.rotate_angle(math.pi/2.0)
"""
