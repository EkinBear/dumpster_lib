#!/usr/bin/env python

from __future__ import division
import rospy
from gazebo_msgs.srv import GetModelState
from gazebo_msgs.srv import GetLinkState
from gazebo_msgs.srv import SetModelState
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import LinkState
from std_srvs.srv import Trigger, TriggerRequest
import math
import sys
from std_msgs.msg import String
from angles import normalize_angle
import time
import os
import requests

class Vector3:
    def __init__(self, x = 0, y = 0, z = 0):
        self.x = x
        self.y = y
        self.z = z

class Quaternion:
    def __init__(self, x = 0, y = 0, z = 0, w = 0):
        self.x = x
        self.y = y
        self.z = z
        self.w = w

    def euler_to_quaternion(self, roll, pitch, yaw):
        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)

        self.w = cr * cp * cy + sr * sp * sy
        self.x = sr * cp * cy - cr * sp * sy
        self.y = cr * sp * cy + sr * cp * sy
        self.z = cr * cp * sy - sr * sp * cy


class EulerAngles:

    def __init__(self, roll = 0, pitch = 0, yaw = 0):
        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

    def quaternion_to_euler_angles(self, q):
        # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        # roll (x-axis rotation)
        sinr_cosp = 2 * (q.w * q.x + q.y * q.z)
        cosr_cosp = 1 - 2 * (q.x * q.x + q.y * q.y)
        self.roll = math.atan2(sinr_cosp, cosr_cosp)

        # pitch (y-axis rotation)
        sinp = 2 * (q.w * q.y - q.z * q.x)
        if abs(sinp) >= 1:
            self.pitch = math.copysign(math.pi/2, sinp)
        else:
            self.pitch = math.asin(sinp)

        # yaw (z-axis rotation)
        siny_cosp = 2 * (q.w * q.z + q.x * q.y)
        cosy_cosp = 1 - 2 * (q.y * q.y + q.z * q.z)
        self.yaw = math.atan2(siny_cosp, cosy_cosp)


class RobotControl:

    def __init__(self, robot_name):

        self.robot_name = robot_name
        self.status = "run"

        self.rotate_right_count = 0
        self.rotate_left_count = 0

        # X Y positions of robot
        self.pos_x = -70
        self.pos_y = 32
        self.prev_pos_x = self.pos_x
        self.prev_pos_y = self.pos_y
        # Direction robot is suppose to face
        # N=>pi/2 E=>0 S=>3*pi/2 W=>pi
        self.dir = 0
        self.prev_dir = self.dir

        self.event_period = 2
        self.msg_rate = 50

        self.robot_pose = String()
        self.state_msg = ModelState()
        self.link_msg = LinkState()

        self.link_msg.reference_frame = "base_link"
        self.wheel_radius = 0.1651
        self.state_msg.model_name = self.robot_name

        rospy.init_node("battery_controller")

        self.robot_pose_pub = rospy.Publisher("/robot_pose/", String, queue_size=10)

        self.set_model_state = rospy.Publisher("/gazebo/set_model_state", ModelState, queue_size=10)
        self.set_link_state = rospy.Publisher("/gazebo/set_link_state", LinkState, queue_size=10)


        self.update(self.pos_x, self.pos_y, self.dir)

        try:
            rospy.wait_for_service("/robot_handler", 5.0)
            self.robot_handle = rospy.ServiceProxy('robot_handler', Trigger)
            resp = self.robot_handle()
            self.status = "run"
        except (rospy.ServiceException, rospy.ROSException), e:
            self.status = "run"
            rospy.logerr("Service call failed: %s" % (e,))


    def get_state(self):

        rospy.wait_for_service('/gazebo/get_model_state')
        try:
            get_model_state = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
            ms = get_model_state(self.robot_name, None)

            model_pose = Vector3(ms.pose.position.x, ms.pose.position.y, ms.pose.position.z)

            q = Quaternion(ms.pose.orientation.x, ms.pose.orientation.y, ms.pose.orientation.z, ms.pose.orientation.w)
            model_rotation = EulerAngles()
            model_rotation.quaternion_to_euler_angles(q)

            return model_pose, model_rotation
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


    def get_link_state(self, link):

        rospy.wait_for_service('/gazebo/get_link_state')
        try:
            get_link_state = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
            ls = get_link_state(link, "base_link")

            link_pose = Vector3(ls.link_state.pose.position.x, ls.link_state.pose.position.y, ls.link_state.pose.position.z)
            q = Quaternion(ls.link_state.pose.orientation.x, ls.link_state.pose.orientation.y, ls.link_state.pose.orientation.z, ls.link_state.pose.orientation.w)
            link_rotation = EulerAngles()
            link_rotation.quaternion_to_euler_angles(q)

            return link_pose, link_rotation
        except rospy.ServiceException, e:
            print "Service call failed: %s"%e


    def polynomial_coef(self, initial_pos, final_pos, final_time, initial_vel = 0, final_vel = 0):

        a0 = initial_pos
        a1 = initial_vel
        a2 = (3/(final_time**2))*(final_pos - initial_pos) - (2/final_time)*initial_vel - final_vel/final_time
        a3 = -(2/(final_time**3))*(final_pos - initial_pos) +  (final_vel - initial_vel)/final_time**2

        return a0, a1, a2, a3


    def pos_on_t(self, t, a0, a1, a2, a3):

        pos = a3*t**3 + a2*t**2 + a1*t + a0
        vel = 3*a3*t**2 + 2*a2*t + a1
        acc = 6*a3*t + 2*a2

        return pos, vel


    def update_wheel(self, link, angle):

        prev_pose, prev_rot = self.get_link_state(link)

        self.link_msg.pose.position.x = prev_pose.x
        self.link_msg.pose.position.y = prev_pose.y
        self.link_msg.pose.position.z = prev_pose.z

        next_angle = normalize_angle(prev_rot.pitch) + angle

        if next_angle >= math.pi/2:
            next_angle = next_angle - math.pi
        if next_angle <= -math.pi/2:
            next_angle = next_angle + math.pi

        q = Quaternion()
        q.euler_to_quaternion(0, next_angle, 0)

        self.link_msg.link_name = link
        self.link_msg.pose.orientation.x = q.x
        self.link_msg.pose.orientation.y = q.y
        self.link_msg.pose.orientation.z = q.z
        self.link_msg.pose.orientation.w = q.w
        self.set_link_state.publish(self.link_msg)


    def move_to_point(self, unit):
        resp = self.robot_handle()
        if not resp.message == "run":
            self.status = "stop"
            return

        if unit > 0:
            wheel_dir = 1
        elif unit < 0:
            wheel_dir = -1

        move_period = self.event_period*abs(unit)


        if not self.pos_x == self.prev_pos_x:
            a0, a1, a2, a3 = self.polynomial_coef(self.prev_pos_x, self.pos_x, move_period)
            self.prev_pos_x = self.pos_x
            t = 0

            while move_period - t >= 0.1:
                pos, vel = self.pos_on_t(t,a0, a1, a2, a3)
                t = t + (1 / self.msg_rate)
                self.update(pos, self.pos_y, self.dir)
                wheel_angle = (-(abs(vel)*wheel_dir) / self.msg_rate) / (2 * self.wheel_radius)
                self.update_wheel("front_left_wheel_link", wheel_angle)
                self.update_wheel("front_right_wheel_link", wheel_angle)
                self.update_wheel("rear_left_wheel_link", wheel_angle)
                self.update_wheel("rear_right_wheel_link", wheel_angle)
                rate = rospy.Rate(self.msg_rate)
                rate.sleep()


        if not self.pos_y == self.prev_pos_y:
            a0, a1, a2, a3 = self.polynomial_coef(self.prev_pos_y, self.pos_y, move_period)
            self.prev_pos_y = self.pos_y
            t = 0

            while move_period - t >= 0.1:
                pos, vel = self.pos_on_t(t,a0, a1, a2, a3)
                t = t + (1 / self.msg_rate)
                self.update(self.pos_x, pos, self.dir)
                wheel_angle = (-(abs(vel)*wheel_dir) / self.msg_rate) / (2 * self.wheel_radius)
                self.update_wheel("front_left_wheel_link", wheel_angle)
                self.update_wheel("front_right_wheel_link", wheel_angle)
                self.update_wheel("rear_left_wheel_link", wheel_angle)
                self.update_wheel("rear_right_wheel_link", wheel_angle)
                rate = rospy.Rate(self.msg_rate)
                rate.sleep()

        self.robot_pose = self.robot_name  + ":" + str(self.pos_x) + ":" + str(self.pos_y)
        self.robot_pose_pub.publish(self.robot_pose)


    def rotate(self, angle):
        resp = self.robot_handle()
        if not resp.message == "run":
            self.status = "stop"
            return

        if angle < 0:
            self.rotate_right_count += 1
        if angle > 0:
            self.rotate_left_count += 1

        if self.rotate_right_count == 2:
            self.achieve(58)
        if self.rotate_left_count == 2:
            self.achieve(59)


        move_period = self.event_period*abs(angle)/math.pi

        error = self.dir - self.prev_dir

        if error > math.pi:
            self.prev_dir = self.prev_dir + math.pi*2
        elif error < -math.pi:
            self.prev_dir = self.prev_dir - math.pi*2

        a0, a1, a2, a3 = self.polynomial_coef(self.prev_dir
        , normalize_angle(self.dir), move_period)
        self.prev_dir = self.dir
        t = 0

        while move_period - t >= 0.1:
            pos, vel = self.pos_on_t(t,a0, a1, a2, a3)
            wheel_angle = ((vel*math.sqrt(0.256000*0.256000)*2) / self.msg_rate) / (2 * self.wheel_radius)
            self.update_wheel("front_left_wheel_link", -wheel_angle)
            self.update_wheel("front_right_wheel_link", wheel_angle)
            self.update_wheel("rear_left_wheel_link", -wheel_angle)
            self.update_wheel("rear_right_wheel_link", wheel_angle)
            t = t + (1 / self.msg_rate)
            self.update(self.pos_x, self.pos_y, pos)
            rate = rospy.Rate(self.msg_rate)
            rate.sleep()


    def update(self, x, y, heading):
        q = Quaternion()
        q.euler_to_quaternion(0, 0, heading)

        self.state_msg.pose.position.x = x
        self.state_msg.pose.position.y = y
        self.state_msg.pose.position.z = 1.02
        self.state_msg.pose.orientation.x = q.x
        self.state_msg.pose.orientation.y = q.y
        self.state_msg.pose.orientation.z = q.z
        self.state_msg.pose.orientation.w = q.w
        self.set_model_state.publish(self.state_msg)


    def move_distance(self, unit):
        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01 or abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            if (not self.pos_x == -70) and (not self.pos_x == 0) and (not self.pos_x == 70):
                print("GarbageRider: Road is blocked cannot move further")
                return

        if abs(normalize_angle(self.dir)) < 0.01 or abs(normalize_angle(self.dir - math.pi)) < 0.01:
            if (not self.pos_y == 32) and (not self.pos_y == 4) and (not self.pos_y == -24) and (not self.pos_y == -52) :
                print("GarbageRider: Road is blocked cannot move further")
                return

        

        if abs(normalize_angle(self.dir - math.pi/2)) < 0.01:
            if (self.pos_y + 1*28*unit) > 46:
                print("GarbageRider: There is no road cannot move further")
                return
            self.pos_y += 1*28*unit

        elif abs(normalize_angle(self.dir)) < 0.01:
            if (self.pos_x + 1*28*unit) > 70:
                print("GarbageRider: There is no road cannot move further")
                return
            self.pos_x += 1*28*unit

        elif abs(normalize_angle(self.dir - math.pi)) < 0.01:
            if (self.pos_x - 1*28*unit) < -70:
                print("GarbageRider: There is no road cannot move further")
                return
            self.pos_x -= 1*28*unit
            
        elif abs(normalize_angle(self.dir + math.pi/2)) < 0.01:
            if (self.pos_y - 1*28*unit) < -52:
                print("GarbageRider: There is no road cannot move further")
                return
            self.pos_y -= 1*28*unit

        if self.status == "run":
            self.move_to_point(unit)


    def rotate_angle(self,angle):
        self.dir += angle
        self.dir = normalize_angle(self.dir)

        if self.status == "run":
            self.rotate(angle)
    
    def achieve(self, achievement_id):
        # get parameters from environment
        host = os.environ.get('RIDERS_HOST', None)
        project_id = os.environ.get('RIDERS_PROJECT_ID', None)
        token = os.environ.get('RIDERS_AUTH_TOKEN', None)

        # generate request url
        achievement_url = '%s/api/v1/project/%s/achievements/%s/achieve/' % (host, project_id, achievement_id)

        # send request
        rqst = requests.post(achievement_url, headers={
            'Authorization': 'Token %s' % token,
            'Content-Type': 'application/json',
        })
    
    def is_ok(self):
        if not self.status == "run":
            return False
        return True
